#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <unistd.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/uio.h>
#include <termios.h>
#include <mosquitto.h>

#include "lib/libt.h"
#include "lib/libe.h"

#define NAME "shellyfwd"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* generic error logging */
#define LOG_MQTT	0x4000000

/* safeguard our LOG_MQTT extension */
#if (LOG_MQTT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_MQTT conflict
#endif
static void mqttlog(int loglevel, const char *str);

static int max_loglevel = LOG_WARNING;
static int logtostderr = -1;

static void set_loglevel(int new_loglevel)
{
	max_loglevel = new_loglevel;
	if (!logtostderr)
		setlogmask(LOG_UPTO(max_loglevel));
}

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel & LOG_PRIMASK, "%s", str);

	} else if ((loglevel & LOG_PRIMASK) <= max_loglevel) {
		struct timespec tv;
		char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}
	if (loglevel & LOG_MQTT) {
		mqttlog(loglevel, str);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": forward shelly MQTT to retained MQTT\n"
	"usage:	" NAME " [OPTIONS ...] SUBSCRIBE[...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --host=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -n, --dryrun		don't actually forward\n"

	" -i, --in=TOPIC	incoming topic, default 'shellies'\n"
	" -o, --out=TOPIC	outgoing topic, default 'remote'\n"
	" -m, --map=IN=OUT	preset map\n"
	" -F, --format=FORMAT	Format string for datetime values, see strftime\n"
	" -H, --homekit[=BOOL]	enable (disable) homekit topics for percentages\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "dryrun", no_argument, NULL, 'n', },

	{ "in", required_argument, NULL, 'i', },
	{ "out", required_argument, NULL, 'o', },

	{ "map", required_argument, NULL, 'm', },
	{ "format", required_argument, NULL, 'F', },
	{ "homekit", optional_argument, NULL, 'H', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:ni:o:m:F:H::";

/* config */
static const char *out_topic = "remote";
static int out_topiclen;
static const char *in_topic = "shellies";
static int in_topiclen;
static const char *tfmt = "%c";
static int homekit;

static int dryrun;
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 0;
static int mqtt_qos = 0;
static int pub_sent_mid, pub_complete_mid;

/* state */
static struct mosquitto *mosq;

#define xfree(x)	({ if (x) free(x); (x) = NULL; })

static void mqtt_pub(const char *topic, const char *payload, int retain);
static const char *payloadfmt(const char *fmt, ...);
static const char *topicfmt(const char *fmt, ...);

/* data */
struct map {
	char *in;
	char *out;
	time_t seen;
	int lost;
	int batlow;
	union {
		struct {
			int boostminutes;
		} trv;
	};
};

static struct map *table;
static int ntable, stable;

static struct map *add_map(const char *in, const char *out)
{
	struct map *map;

	if (ntable >= stable) {
		stable += 16;
		table = realloc(table, sizeof(*table)*stable);
		if (!table)
			mylog(LOG_ERR, "realloc table %i: %s", stable, ESTR(errno));
	}
	map = table+ntable++;
	memset(map, 0, sizeof(*map));
	map->in = strdup(in);
	map->out = strdup(out);
	return map;
}

static struct map *find_map(const char *in, const char *out)
{
	int j;

	for (j = 0; j < ntable; ++j) {
		if (in && strcmp(in, table[j].in))
			continue;
		if (out && strcmp(out, table[j].out))
			continue;
		return table+j;
	}
	return NULL;
}

static void del_map(struct map *map)
{
	mylog(LOG_ERR, "%s impossible now", __func__);
}

/* cache */
struct cache {
	char *topic;
	char *payload;
};

static struct cache *caches;
static int ncache, scache;

static int cached(const char *topic, const char *payload)
{
	int j;

	for (j = 0; j < ncache; ++j) {
		if (strcmp(caches[j].topic, topic))
			continue;
		if (!strcmp(caches[j].payload ?: "", payload ?: ""))
			return 1;
		goto diff;
	}
	if (ncache >= scache) {
		scache += 16;
		caches = realloc(caches, sizeof(*caches)*scache);
		if (!caches)
			mylog(LOG_ERR, "realloc %i cache: %s", scache, ESTR(errno));
	}
	caches[j].topic = strdup(topic);
	caches[j].payload = NULL;
	++ncache;
diff:
	if (caches[j].payload)
		free(caches[j].payload);
	caches[j].payload = payload ? strdup(payload) : NULL;
	return 0;
}

/* signal handler */
static volatile int sigterm;

static void signalrecvd(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		}
	}
}

/* usefull tricks */
__attribute__((unused))
static double strtod_safe(const char *str)
{
	double val;
	char *endp;

	val = strtod(str, &endp);
	if (endp > str)
		return val;
	return NAN;
}

/* mqtt iface */
static char *topicfmt_str;
__attribute__((format(printf,1,2)))
static const char *topicfmt(const char *fmt, ...)
{
	va_list va;

	xfree(topicfmt_str);
	va_start(va, fmt);
	vasprintf(&topicfmt_str, fmt, va);
	va_end(va);
	return topicfmt_str;
}
static char *payloadfmt_str;
__attribute__((unused))
__attribute__((format(printf,1,2)))
static const char *payloadfmt(const char *fmt, ...)
{
	va_list va;

	xfree(payloadfmt_str);
	va_start(va, fmt);
	vasprintf(&payloadfmt_str, fmt, va);
	va_end(va);
	if (!strcmp("nan", payloadfmt_str))
		return "";
	return payloadfmt_str;
}

static void mqtt_pub(const char *topic, const char *payload, int retain)
{
	int ret;

	if (retain) {
		if (cached(topic, payload))
			/* TODO: only update cache after successful send */
			return;
	}
	if (dryrun) {
		mylog(LOG_INFO, "mqtt:>%s %s", topic, payload ?: "");
	} else {
		mylog(LOG_DEBUG, "mqtt:>%s %s", topic, payload ?: "<null>");
		ret = mosquitto_publish(mosq, &pub_sent_mid, topic, strlen(payload?:""), payload, mqtt_qos, retain);
		if (ret < 0)
			mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
	}
}

static void my_mqtt_pub(struct mosquitto *mosq, void *dat, int mid)
{
	pub_complete_mid = mid;
}

static inline int mqtt_pending(void)
{
	return pub_sent_mid != pub_complete_mid;
}

static void mqtt_sub(const char *topic)
{
	int ret;

	mylog(LOG_INFO, "subscribe %s", topic);
	ret = mosquitto_subscribe(mosq, NULL, topic, mqtt_qos);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", topic, mosquitto_strerror(ret));
}

static void mqttlog(int loglevel, const char *msg)
{
	static const char *const prionames[] = {
		[LOG_EMERG] = "emerg",
		[LOG_ALERT] = "alert",
		[LOG_CRIT] = "crit",
		[LOG_ERR] = "err",
		[LOG_WARNING] = "warn",
		[LOG_NOTICE] = "notice",
		[LOG_INFO] = "info",
		[LOG_DEBUG] = "debug",
	};

	mqtt_pub(topicfmt("log/%s/%s/%s", NAME, prionames[loglevel & LOG_PRIMASK], "p1"), msg, 0);
}

static void my_mqtt_log(struct mosquitto *mosq, void *userdata, int level, const char *str)
{
	static const int logpri_map[] = {
		MOSQ_LOG_ERR, LOG_ERR,
		MOSQ_LOG_WARNING, LOG_WARNING,
		MOSQ_LOG_NOTICE, LOG_NOTICE,
		MOSQ_LOG_INFO, LOG_INFO,
		MOSQ_LOG_DEBUG, LOG_DEBUG,
		0,
	};
	int j;

	for (j = 0; logpri_map[j]; j += 2) {
		if (level & logpri_map[j]) {
			//mylog(logpri_map[j+1], "[mosquitto] %s", str);
			return;
		}
	}
}

static void free_remote(const char *remote)
{
	/* remove topics */
	const char *topic = topicfmt("%s/%s/", out_topic, remote);
	int len = strlen(topic);
	int j;

	for (j = 0; j < ntable; ++j) {
		if (!strncmp(table[j].out, topic, len))
			mqtt_pub(table[j].out, "", 1);
	}
}

/* extract JSON */
#include "jsmn/jsmn.h"

static jsmntok_t *jsontoks;
static size_t jsontokcnt;

static int get_json_recursive(const char *needle, const char *json, jsmntok_t *t, const char *topic, char **presult)
{
	int ntok, j;
	char *newtopic;
	size_t len;

	switch (t->type) {
	case JSMN_PRIMITIVE:
	case JSMN_STRING:
		if (!strcmp(needle, topic)) {
			*presult = realloc(*presult, t->end - t->start + 1);
			strncpy(*presult, json + t->start, t->end - t->start);
			(*presult)[t->end - t->start] = 0;
		}
		return 1;
	case JSMN_OBJECT:
		newtopic = NULL;
		len = 0;
		ntok = 1;
		for (j = 0; j < t->size; ++j) {
			if (t[ntok].type != JSMN_STRING)
				mylog(LOG_ERR, "property with non-string name");
			if (strlen(topic) + t[ntok].end - t[ntok].start + 1 > len) {
				len = (strlen(topic) + t[ntok].end - t[ntok].start + 1 + 128) & ~127;
				newtopic = realloc(newtopic, len);
				if (!newtopic)
					mylog(LOG_ERR, "realloc %li: %s", (long)len, ESTR(errno));
			}
			sprintf(newtopic, "%s/%.*s", topic, t[ntok].end - t[ntok].start, json + t[ntok].start);
			++ntok;
			ntok += get_json_recursive(needle, json, t+ntok, *topic ? newtopic : newtopic+1, presult);
		}
		if (newtopic)
			free(newtopic);
		return ntok;
	case JSMN_ARRAY:
		newtopic = malloc(strlen(topic) + 16);
		ntok = 1;
		for (j = 0; j < t->size; ++j) {
			sprintf(newtopic, "%s/%i", topic, j);
			ntok += get_json_recursive(needle, json, t+ntok, newtopic, presult);
		}
		free(newtopic);
		return ntok;
	default:
		return 0;
	}
}

static const char *get_json(const char *needle, const char *jsonstr)
{
	static char *result;

	if (result)
		*result = 0;
	get_json_recursive(needle, jsonstr, jsontoks, "", &result);
	return (result && *result) ? result : NULL;
}

static int recvd_json(const char *json)
{
	/* start parsing */
	jsmn_parser prs;
	int len, ret;

	len = strlen(json);
	jsmn_init(&prs);
	ret = jsmn_parse(&prs, json, len, NULL, 0);
	if (ret < 0)
		return ret;

	jsmn_init(&prs);
	if (ret > jsontokcnt) {
		jsontokcnt = (ret + 7) & ~7;
		jsontoks = realloc(jsontoks, sizeof(*jsontoks)*jsontokcnt);
	}
	return jsmn_parse(&prs, json, len, jsontoks, jsontokcnt);
}

/* mqtt */
static void shelly_lost(void *dat)
{
	struct map *map = dat;

	mqtt_pub(topicfmt("%s/%s/online", out_topic, map->out), "0", 1);
	mqtt_pub(topicfmt("%s/alert", out_topic), payloadfmt("%s lost", map->out), 0);
	map->lost = 1;
}

static double shelly_timeout(const struct map *map)
{
	if (!fnmatch("shellybutton1-*", map->in, FNM_PATHNAME))
		return 3600*24*30;
	else if (!fnmatch("shellyht-*", map->in, FNM_PATHNAME))
		return 3600*12;
	else if (!fnmatch("shellytrv-*", map->in, FNM_PATHNAME))
		return 90*60;
	else
		return 120;
}
static double shelly_batlow(const struct map *map)
{
	if (!fnmatch("shellybutton1-*", map->in, FNM_PATHNAME))
		return 0.4;
	else if (!fnmatch("shellyht-*", map->in, FNM_PATHNAME))
		return 0.85;
	else if (!fnmatch("shellytrv-*", map->in, FNM_PATHNAME))
		return 0.85;
	else
		return NAN;
}

static void pub_seen(struct map *map)
{
	static char buf[128];
	time_t now;

	time(&now);
	if (now == map->seen)
		return;
	if (map->lost)
		mqtt_pub(topicfmt("%s/alert", out_topic), payloadfmt("%s resumed", map->out), 0);
	if (!map->seen || map->lost)
		/* first time, or resumed */
		mqtt_pub(topicfmt("%s/%s/online", out_topic, map->out), "1", 1);
	libt_add_timeout(shelly_timeout(map), shelly_lost, map);
	map->lost = 0;
	mqtt_pub(topicfmt("%s/%s/seen", out_topic, map->out), payloadfmt("%lu", now), 1);
	strftime(buf, sizeof(buf), tfmt, localtime(&now));
	mqtt_pub(topicfmt("%s/%s/seen/local", out_topic, map->out), buf, 1);
	map->seen = now;
}

static struct boolmap {
	int value;
	const char *str;
} const boolmaps[] = {
	{ 1, "true", },
	{ 0, "false", },
	{ 1, "on", },
	{ 0, "off", },
};
static int strtobool(const char *str, char **pendp)
{
	char *endp;
	int result;
	int j, len;

	if (!pendp)
		pendp = &endp;

	result = strtol(str, pendp, 0);
	if (*pendp > str)
		return result;

	for (j = 0; j < sizeof(boolmaps)/sizeof(boolmaps[0]); ++j) {
		len = strlen(boolmaps[j].str);
		if (!strncasecmp(str, boolmaps[j].str, len)) {
			*pendp = (char *)str+len;
			return boolmaps[j].value;
		}
	}
	*pendp = (char *)str;
	return 0;
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	char *topic, *payload;
	char *id;
	char *tok;
	struct map *map;
	int j;
	int idx;

	payload = msg->payloadlen ? (char *)msg->payload : NULL;
	mylog(LOG_DEBUG, "mqtt:<%s %s", msg->topic, payload ?: "<null>");

	if (!strncmp(msg->topic, out_topic, out_topiclen)) {
		topic = msg->topic+out_topiclen+1;
		if (!fnmatch("*/id", topic, FNM_PATHNAME)) {
			/* strip /id suffix */
			topic[strlen(topic)-3] = 0;
			if (msg->payloadlen) {
				map = find_map(payload, NULL);
				if (map && strcmp(map->out, topic)) {
					free_remote(map->out);
					del_map(map);
				}
				if (!map || strcmp(map->out, topic))
					add_map(payload, topic);

			} else {
				del_map(find_map(payload, topic));
				free_remote(payload);
			}

		} else if (!strncmp(topic, "cfg/", 4)) {
			topic += 4;

			if (!strcmp(topic, "loglevel")) {
				set_loglevel(strtol(payload, NULL, 0));
				mylog(LOG_WARNING, "loglevel %i", max_loglevel);
			}

		} else {
			/* lookup entry */
			id = topic;
			topic = strchr(topic, '/');
			if (!topic)
				return;
			*topic++ = 0;
			map = find_map(NULL, id);
			if (!map)
				return;
			tok = strchr(topic, '/');
			idx = strtoul(tok ? tok+1 : "0", NULL, 0);

			if (!fnmatch("relay/*/set", topic, FNM_PATHNAME)) {
				mqtt_pub(topicfmt("%s/%s/relay/%i/command", in_topic, map->in, idx),
						((payload ?: "0")[0] == '0') ? "off" : "on", 0);

			} else if (!fnmatch("thermostats/*/setp/set", topic, FNM_PATHNAME)) {
				/* mind 'thermostat' != 'thermostats' */
				mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/target_t", in_topic, map->in, idx),
						payload, 0);

			} else if (!fnmatch("thermostats/*/pos/set", topic, FNM_PATHNAME)) {
				/* mind 'thermostat' != 'thermostats' */
				mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/valve_pos", in_topic, map->in, idx),
						payloadfmt("%.0f", strtod(payload ?: "", NULL) * 100), 0);

			} else if (!fnmatch("thermostats/*/schedule/set", topic, FNM_PATHNAME)) {
				if (payload)
					mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/schedule_profile", in_topic, map->in, idx),
							payload, 0);
				mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/schedule", in_topic, map->in, idx),
						payload ? "1": "0", 0);

			} else if (!fnmatch("thermostats/*/boost/set", topic, FNM_PATHNAME)) {
				/* mind 'thermostat' != 'thermostats' */
				mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/boost_minutes", in_topic, map->in, idx),
						/* set boost time to 60min */
						strtol(payload ?: "", NULL, 0) ? "60" : "0", 0);

			} else if (!fnmatch("thermostats/*/ext_t", topic, FNM_PATHNAME)) {
				/* mind 'thermostat' != 'thermostats' */
				mqtt_pub(topicfmt("%s/%s/thermostat/%i/command/ext_t", in_topic, map->in, idx),
						payload, 0);
			}
		}
	} else if (!strncmp(msg->topic, in_topic, in_topiclen)) {
		topic = msg->topic+in_topiclen+1;
		id = topic;
		topic = strchr(topic, '/');
		if (!topic)
			return;
		*topic++ = 0;
		map = find_map(id, NULL);
		if (!map) {
			char *mac = strrchr(id, '-');
			if (mac)
				map = add_map(id, mac+1);
		}

		static const char *const fwd_patterns[] = {
			"*/*/power",
			"*/*/energy",
			"*/*/total",
			"sensor/*",
			"temperature",
			"overtemperature",
			"input/*",
		};

		if (!strcmp("info", topic)) {
			double value;
			int ival;

			if (!fnmatch("shellytrv-*", map->in, FNM_PATHNAME)) {
				if (!payload)
					payload = "";
				if (recvd_json(payload) < 0)
					return;
				/* publish items from json */
				value = strtod(get_json("bat/value", payload) ?: "nan", NULL);
				mqtt_pub(topicfmt("%s/%s/sensor/battery", out_topic, map->out),
						payloadfmt("%.2f", value/100), 1);
				if (homekit)
					mqtt_pub(topicfmt("%s/%s/sensor/battery/homekit", out_topic, map->out), payloadfmt("%.0f", value), 1);
				if (value/100 < shelly_batlow(map)) {
					if (!map->batlow)
						mqtt_pub(topicfmt("%s/alert", out_topic), payloadfmt("%s: battery low %.2f", map->out, value/100), 0);
					map->batlow = 1;
				} else {
					map->batlow = 0;
				}
				/* real battery voltage */
				mqtt_pub(topicfmt("%s/%s/sensor/battery/voltage", out_topic, map->out),
						get_json("bat/voltage", payload), 1);
				ival = strtobool(get_json("charger", payload) ?: "", NULL);
				mqtt_pub(topicfmt("%s/%s/charger", out_topic, map->out),
						payloadfmt("%i", ival), 1);

				value = strtod(get_json("thermostats/0/pos", payload) ?: "nan", NULL);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/pos", out_topic, map->out),
						payloadfmt("%.2f", value/100), 1);
				if (homekit)
					mqtt_pub(topicfmt("%s/%s/thermostats/0/pos/homekit", out_topic, map->out), payloadfmt("%.0f", value), 1);
				ival = strtobool(get_json("thermostats/0/tmp/is_valid", payload) ?: "", NULL);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/temperature", out_topic, map->out),
						ival ? get_json("thermostats/0/tmp/value", payload) : "", 1);
				ival = strtobool(get_json("thermostats/0/target_t/enabled", payload) ?: "", NULL);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/setp", out_topic, map->out),
						ival ? get_json("thermostats/0/target_t/value", payload) : "", 1);
				ival = strtobool(get_json("thermostats/0/schedule", payload) ?: "", NULL);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/schedule", out_topic, map->out),
						ival ? get_json("thermostats/0/schedule_profile", payload) : "", 1);
				ival = strtol(get_json("thermostats/0/boost_minutes", payload) ?: "", NULL, 0);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/boost", out_topic, map->out),
						ival ? "1" : "0", 1);
				mqtt_pub(topicfmt("%s/%s/thermostats/0/boost/minutes", out_topic, map->out),
						ival ? payloadfmt("%i", ival) : "", 1);
				pub_seen(map);
			}

		} else if (!fnmatch("relay/*", topic, FNM_PATHNAME)) {
			payload = !strcmp(payload ?: "", "on") ? "1" : "0";
			mqtt_pub(topicfmt("%s/%s/%s", out_topic, map->out, topic), payload, 1);

			if (!strcmp("relay/0", topic))
				pub_seen(map);

		} else if (!strcmp("sensor/act_reasons", topic)) {
			return;

		} else if (!strcmp("sensor/battery", topic)) {
			double value = strtod(payload ?: "", 0);
			mqtt_pub(topicfmt("%s/%s/%s", out_topic, map->out, topic), payloadfmt("%.2f", value/100), 1);
			if (homekit)
				mqtt_pub(topicfmt("%s/%s/%s/homekit", out_topic, map->out, topic), payloadfmt("%.0f", value), 1);
			if (value/100 < shelly_batlow(map)) {
				if (!map->batlow)
					mqtt_pub(topicfmt("%s/alert", out_topic), payloadfmt("%s: battery low %.2f", map->out, value/100), 0);
				map->batlow = 1;
			} else {
				map->batlow = 0;
			}

		} else if (!strcmp("sensor/humidity", topic)) {
			double value = strtod(payload ?: "", 0);
			mqtt_pub(topicfmt("%s/%s/%s", out_topic, map->out, topic), payloadfmt("%.2f", value/100), 1);
			if (homekit)
				mqtt_pub(topicfmt("%s/%s/%s/homekit", out_topic, map->out, topic), payloadfmt("%.0f", value), 1);

		} else if (!fnmatch("input_event/*", topic, FNM_PATHNAME)) {
			if (recvd_json(payload ?: "") < 0)
				return;
			const char *newtopic;
			newtopic = topicfmt("%s/%s/%s/%s", out_topic, map->out, topic, get_json("event", payload ?: ""));
			mqtt_pub(newtopic, "1", 0);

		} else for (j = 0; j < sizeof(fwd_patterns)/sizeof(fwd_patterns[0]); ++j) {
			if (!fnmatch(fwd_patterns[j], topic, FNM_PATHNAME)) {
				mqtt_pub(topicfmt("%s/%s/%s", out_topic, map->out, topic), payload, 1);
				pub_seen(map);
				break;
			}
		}
	}
}

static void mqtt_maintenance(void *dat)
{
	int ret;
	struct mosquitto *mosq = dat;

	ret = mosquitto_loop_misc(mosq);
	if (ret)
		mylog(LOG_ERR, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
	libt_add_timeout(2.3, mqtt_maintenance, dat);
}

static void recvd_mosq(int fd, void *dat)
{
	struct mosquitto *mosq = dat;
	int evs = libe_fd_evs(fd);
	int ret;

	if (evs & LIBE_RD) {
		/* mqtt read ... */
		ret = mosquitto_loop_read(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
	}
	if (evs & LIBE_WR) {
		/* flush mqtt write queue _after_ the timers have run */
		ret = mosquitto_loop_write(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
	}
}

void mosq_update_flags(void)
{
	if (mosq)
		libe_mod_fd(mosquitto_socket(mosq), LIBE_RD | (mosquitto_want_write(mosq) ? LIBE_WR : 0));
}

int main(int argc, char *argv[])
{
	int opt, ret;
	char *str;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
	opt_fail:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'h':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'n':
		dryrun = 1;
		break;

	case 'i':
		in_topic = optarg;
		break;
	case 'o':
		out_topic = optarg;
		break;
	case 'm':
		str = strchr(optarg, '=');
		if (!str)
			goto opt_fail;
		*str++ = 0;
		add_map(optarg, str);
		break;
	case 'F':
		tfmt = optarg;
		break;
	case 'H':
		homekit = strtol(optarg ?: "1", NULL, 0);
		break;
	}

	/* MQTT start */
	in_topiclen = strlen(in_topic);
	out_topiclen = strlen(out_topic);
	mosquitto_lib_init();
	mosq = mosquitto_new(topicfmt(NAME "-%i", getpid()), true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_log_callback_set(mosq, my_mqtt_log);
	mosquitto_message_callback_set(mosq, my_mqtt_msg);
	mosquitto_publish_callback_set(mosq, my_mqtt_pub);
	mosquitto_will_set(mosq, topicfmt("%s/state", out_topic), 4, "lost", mqtt_qos, 1);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive ?: 60);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* subscribe to topics */
	/* order matters:
	 * first subscribe to id's so they are known when the in_topic arrives
	 */
	mqtt_sub(topicfmt("%s/+/id", out_topic));
	mqtt_sub(topicfmt("%s/+/relay/+/set", out_topic));
	mqtt_sub(topicfmt("%s/+/thermostats/+/setp/set", out_topic));
	mqtt_sub(topicfmt("%s/+/thermostats/+/pos/set", out_topic));
	mqtt_sub(topicfmt("%s/+/thermostats/+/schedule/set", out_topic));
	mqtt_sub(topicfmt("%s/+/thermostats/+/boost/set", out_topic));
	mqtt_sub(topicfmt("%s/+/thermostats/+/ext_t", out_topic));
	if (optind >= argc) {
		mqtt_sub(topicfmt("%s/#", in_topic));
	} else for (; optind < argc; ++optind) {
		mqtt_sub(argv[optind]);
	}

	libt_add_timeout(0, mqtt_maintenance, mosq);
	libe_add_fd(mosquitto_socket(mosq), recvd_mosq, mosq);

	/* prepare signalfd */
	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);
	/* for inside GDB */
	//sigdelset(&sigmask, SIGINT);

	if (sigprocmask(SIG_BLOCK, &sigmask, NULL) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, signalrecvd, NULL);

	mqtt_pub(topicfmt("%s/state", out_topic), "on", 1);
	/* core loop */
	for (; !sigterm;) {
		libt_flush();
		mosq_update_flags();
		if (libe_wait(libt_get_waittime()) >= 0)
			libe_flush();

	}
	mylog(LOG_NOTICE, "terminating ...");
	if (!mqtt_qos)
		/* set qos >= 1, so that all is flushed */
		mqtt_qos = 1;

	mqtt_pub(topicfmt("%s/state", out_topic), "off", 1);

	/* terminate */
	for (; mqtt_pending(); ) {
		libt_flush();
		mosq_update_flags();
		if (libe_wait(libt_get_waittime()) >= 0)
			libe_flush();
	}
#if 1
	xfree(payloadfmt_str);
	xfree(topicfmt_str);

	/* cleanup */
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return 0;
}
